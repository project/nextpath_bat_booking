<?php

namespace Drupal\nextpath_bat_booking\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'simple booking form' formatter.
 *
 * @FieldFormatter(
 *   id = "nextpath_simple_booking_form",
 *   label = @Translation("Simple Booking Form"),
 *   description = @Translation("Simple Booking Form."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SimpleBookingFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity->id() && $entity->getEntityType()->id() && $entity->getEntityType()->id() == 'bat_unit') {
        $elements[$delta] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => 'nextpath_simple_booking_form',
            'data-target-id' => $entity->id(),
          ],
        ];
      }
    }
    $elements['#attached']['library'][] = 'nextpath_bat_booking/global-scripts';
    $elements['#attached']['drupalSettings']['path']['getHost'] = \Drupal::request()->getSchemeAndHttpHost();

    return $elements;
  }

}
