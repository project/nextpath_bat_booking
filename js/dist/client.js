/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/client.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/client.js":
/*!***********************!*\
  !*** ./src/client.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("window.nextpath_client = function () {\n  function getCsrfToken(callback) {\n    return fetch('/rest/session/token', {\n      method: 'get',\n      headers: {\n        'Accept': 'application/json'\n      }\n    }).then(checkStatus).then(parseText).then(callback);\n  }\n\n  function checkAvailability(unit, startDate, endDate, success) {\n    return fetch('/nextpath-availability/' + unit + '/' + startDate + '/' + endDate, {\n      headers: {\n        Accept: 'application/json'\n      }\n    }).then(checkStatus).then(parseJSON).then(success);\n  }\n\n  function createBooking(data, success) {\n    getCsrfToken(function (csrfToken) {\n      return fetch('/entity/bat_event?_format=hal_json', {\n        method: 'post',\n        body: JSON.stringify(data),\n        headers: {\n          'Content-Type': 'application/hal+json',\n          'X-CSRF-Token': csrfToken\n        }\n      }).then(checkStatus).then(parseJSON).then(success);\n    });\n  }\n\n  function getBookingHistory(unit, success) {\n    return fetch('/nextpath-booking-history/' + unit, {\n      headers: {\n        Accept: 'application/json'\n      }\n    }).then(checkStatus).then(parseJSON).then(success);\n  }\n\n  function checkStatus(response) {\n    if (response.status >= 200 && response.status < 300) {\n      return response;\n    } else {\n      var error = new Error(\"HTTP Error \".concat(response.statusText));\n      error.status = response.statusText;\n      error.response = response;\n      console.log(error);\n      throw error;\n    }\n  }\n\n  function parseJSON(response) {\n    return response.json();\n  }\n\n  function parseText(response) {\n    return response.text();\n  }\n\n  function reverseArray(response) {\n    return response.reverse();\n  }\n\n  return {\n    checkAvailability: checkAvailability,\n    createBooking: createBooking,\n    getBookingHistory: getBookingHistory\n  };\n}();\n\n//# sourceURL=webpack:///./src/client.js?");

/***/ })

/******/ });