window.nextpath_client = (function () {
  function getCsrfToken(callback) {
    return fetch('/rest/session/token', {
      method: 'get',
      headers: {
        'Accept': 'application/json',
      },
    }).then(checkStatus)
      .then(parseText)
      .then(callback);
  }

  function checkAvailability(unit, startDate, endDate, success) {
    return fetch('/nextpath-availability/' + unit + '/' + startDate + '/' + endDate, {
      headers: {
        Accept: 'application/json',
      },
    }).then(checkStatus)
      .then(parseJSON)
      .then(success);
  }

  function createBooking(data, success) {
    getCsrfToken(function (csrfToken) {
      return fetch('/entity/bat_event?_format=hal_json', {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/hal+json',
          'X-CSRF-Token': csrfToken,
        },
      }).then(checkStatus)
        .then(parseJSON)
        .then(success);
    });
  }

  function getBookingHistory(unit, success) {
    return fetch('/nextpath-booking-history/' + unit, {
      headers: {
        Accept: 'application/json',
      },
    }).then(checkStatus)
      .then(parseJSON)
      .then(success);
  }

  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      const error = new Error(`HTTP Error ${response.statusText}`);
      error.status = response.statusText;
      error.response = response;
      console.log(error);
      throw error;
    }
  }

  function parseJSON(response) {
    return response.json();
  }

  function parseText(response) {
    return response.text();
  }

  function reverseArray(response) {
    return response.reverse();
  }

  return {
    checkAvailability,
    createBooking,
    getBookingHistory,
  };
}());
