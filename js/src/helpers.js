window.nextpath_helpers = (function () {
  function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  }

  function formatDate(date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    //var seconds = date.getSeconds();
    var seconds = 0;
    return pad(year, 4) + '-' + pad(month, 2) + '-' + pad(day, 2) + 'T' + pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
  }

  function createBooking(targetId, startDate, endDate) {
    const booking = {
      _links: {
        type: {
          href: drupalSettings.path.getHost + '/rest/type/bat_event/room_availability'
        }
      },
      type: {
        target_id: 'room_availability'
      },
      event_bat_unit_reference: {
        0: {
          target_id: 3,
        },
      },
      event_dates: {
        0: {
          value: startDate,
          end_value: endDate,
        }
      },
      event_state_reference: {
        0: {
          target_id: 2,
        }
      },
    };

    return booking;
  }

  return {
    formatDate,
    pad,
    createBooking,
  };
}());
