import React from 'react';
import ReactDOM from 'react-dom';

class Booking extends React.Component {
  render() {
    return (
    <div className={"booking-history-row"}>
        <div className="period">
          Period: {this.props.period}
        </div>
        <div className="created">
          Bokked on: {this.props.created}
        </div>
     </div>
    );
  }
}

export default Booking;
