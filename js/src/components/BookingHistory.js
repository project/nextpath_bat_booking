import React from 'react';
import ReactDOM from 'react-dom';

import Booking from './Booking';

class BookingHistory extends React.Component {
  render() {
    const bookings = this.props.bookingHostory.map((booking) => (
      <Booking
        key={booking.id}
        id={booking.id}
        period={booking.event_dates}
        created={booking.created}
      />
    ));
    return (
      <div>
        <div className="booking-history-container">
          {bookings}
        </div>
      </div>
    );
  }
}

export default BookingHistory;
