import React from 'react';
import ReactDOM from 'react-dom';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import BookingHistory from './BookingHistory';

class BookingWindow extends React.Component {
  constructor(props) {
    super(props);
    // Set start date (tomorrow).
    var startDate = new Date();
    startDate.setDate(startDate.getDate() + 1);
    // Set end date (tomorrow + 1h).
    var endDate = new Date();
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(endDate.getHours() + 1);

    this.state = {
      startDate: startDate,
      endDate: endDate,
      buttonDisabled: true,
      statusMessage: 'Select the period to check the availability.',
      statusClass: 'booking-status-message-netrual',
      bookingHostory: [],
    };
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getBookingHistory();
  }

  handleStartChange(date) {
    // Make sure endDate is to before startDate,
    if (date >= this.state.endDate) {
      let endDate = date;
      endDate.setHours(endDate.getHours() + 1);
      this.setState({
        endDate: endDate
      });
    }

    this.setState({
      startDate: date
    });
    this.checkAvailabilty(date, this.state.endDate);
  };

  handleEndChange(date) {
    // Make sure endDate is to before startDate,
    if (date < this.state.startDate) {
      date = this.state.startDate;
    }

    this.setState({
      endDate: date
    });
    this.checkAvailabilty(this.state.startDate, date);
  };

  checkAvailabilty(startDate, endDate) {
    this.setState({
      statusMessage: 'Looking up for availability...'
    });

    nextpath_client.checkAvailability(this.props.dataTargetId, nextpath_helpers.formatDate(startDate), nextpath_helpers.formatDate(endDate), (bookings) => (
      this.setState({
        buttonDisabled: bookings.length > 0 ? true : false,
        statusMessage: bookings.length > 0 ? 'Unit already booked in the selected period' : 'Unit available for booking',
        statusClass: bookings.length > 0 ? 'booking-status-message-booked' : 'booking-status-message-available'
      })
    ));
  }

  handleSubmit() {
    const b = nextpath_helpers.createBooking(this.props.dataTargetId, nextpath_helpers.formatDate(this.state.startDate), nextpath_helpers.formatDate(this.state.endDate));
    nextpath_client.createBooking(b, (reply) => (
      this.checkSubmitResult(reply)
    ));
  };

  checkSubmitResult(reply) {
    if(reply.hasOwnProperty('id')){
      this.getBookingHistory();
      this.setState({
        buttonDisabled: true,
        statusMessage: 'Booking was successful',
        statusClass: 'booking-status-message-created',
      })
    }
  }

  getBookingHistory() {
    nextpath_client.getBookingHistory(this.props.dataTargetId, (bookings) => (
      this.setState({
        bookingHostory: bookings,
      })
    ));
  }

  render() {
    return (
      <div className="booking-window">
        <div className="booking-date-selector">
          <DatePicker
            selected={this.state.startDate}
            selectsStart
            onChange={this.handleStartChange}
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={15}
            dateFormat="dd.MM.yyyy HH:mm"
            timeCaption="time"
          />
          <DatePicker
            selected={this.state.endDate}
            selectsEnd
            onChange={this.handleEndChange}
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={15}
            dateFormat="dd.MM.yyyy HH:mm"
            timeCaption="time"
          />
        </div>
        <div className={this.state.statusClass}>
          {this.state.statusMessage}
        </div>
        <div className='booking-button'>
          <button
            onClick={this.handleSubmit}
            disabled={this.state.buttonDisabled}
          >
           Book
          </button>
        </div>
        <div className="booking-history">
          <BookingHistory
            bookingHostory={this.state.bookingHostory}
          />
        </div>
      </div>
    );
  }
}

export default BookingWindow;
