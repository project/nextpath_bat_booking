import React from 'react';
import ReactDOM from 'react-dom';

import BookingWindow from './components/BookingWindow';

Array.prototype.forEach.call(document.getElementsByClassName('nextpath_simple_booking_form'), function(element) {
  ReactDOM.render(
    <BookingWindow dataTargetId={element.getAttribute('data-target-id')} />,
    element
  );
});
